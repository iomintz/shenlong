import React from 'react';
import ReactDOM from 'react-dom';

import './index.css';
import App from './components/App';
import register from './icons.js';

register();

ReactDOM.render(<App />, document.getElementById('root'));
